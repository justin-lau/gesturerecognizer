#pragma once

#include <opencv2\opencv.hpp>

class Color
{
protected:
	int r;
	int g;
	int b;

public:
	static const Color &Black;
	static const Color &Red;
	static const Color &Green;
	static const Color &Blue;
	static const Color &Yellow;
	static const Color &Purple;
	static const Color &Cyan;
	static const Color &White;

	Color(const int &r, const int &g, const int &b);

	const cv::Scalar toScalar() const;
	const Color getInvert() const;
};

