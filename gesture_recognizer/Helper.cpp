#include "Helper.h"

const int Helper::Int::trim(int value, const int &lowerBound, const int &upperBound){
	while(value < lowerBound)
		value = upperBound - (lowerBound - value) + 1;

	while(value > upperBound)
		value = value - upperBound + lowerBound - 1;

	return value;
}

// Returns the mid point of points a and b.
const cv::Point2f Helper::Point::middleOf(const cv::Point &a, const cv::Point &b){
	return (a + b) * 0.5;
}

// Returns the distance point of points a and b.
const float Helper::Point::distance(const cv::Point &a, const cv::Point &b){
	return sqrt(pow(a.x - b.x, 2) + pow(a.y - b.y, 2));
}

const cv::Point Helper::Point::extendedPoint(const cv::Point &a, const cv::Point &b){
	return b * 2 - a;
}

// Finding the angle between line AB and line AC
const float Helper::Triangle::degreeA(const cv::Point &pointA, const cv::Point &pointB, const cv::Point &pointC){
	float lengthAB = Helper::Point::distance(pointA, pointB);
	float lengthAC = Helper::Point::distance(pointA, pointC);
	float lengthBC = Helper::Point::distance(pointB, pointC);

	return acosf((pow(lengthAB, 2) + pow(lengthAC, 2) - pow(lengthBC, 2)) / (2 * lengthAB * lengthAC)) * 180 / CV_PI;
}

// Returns the index of the element who has the highest value,
// or -1 if vector is empty.
const int Helper::Vector::Float::maxIndex(const std::vector<float> &vector){
	int size = vector.size();

	if(size == 0)
		return -1;

	int maxIndex = 0;
	float maxValue = vector[0];

	for(int i = 1; i < size; ++i){
		if(vector[i] > maxValue){
			maxValue = vector[i];
			maxIndex = i;
		}
	}

	return maxIndex;
}

// Returns the index of the element who has the lowest value,
// or -1 if vector is empty.
const int Helper::Vector::Float::minIndex(const std::vector<float> &vector){
	int size = vector.size();

	if(size == 0)
		return -1;

	int minIndex = 0;
	float minValue = vector[0];

	for(int i = 1; i < size; ++i){
		if(vector[i] < minValue){
			minValue = vector[i];
			minIndex = i;
		}
	}

	return minIndex;
}

// Returns a vector of distances calculated from each points in `origins` to `destination`
const std::vector<float> Helper::Vector::Point::distances(const std::vector<cv::Point> &origins, const cv::Point &destination){
	int size = origins.size();

	std::vector<float> distances(size);

	for(int i = 0; i < size; ++i)
		distances[i] = Helper::Point::distance(origins[i], destination);

	return distances;
}

// Returns the minimum distance calculated from each points in `origins` to `destination`,
// or -1 if vector is empty.
const float Helper::Vector::Point::minDistance(const std::vector<cv::Point> &origins, const cv::Point &destination){
	if(origins.empty())
		return -1;

	std::vector<float> distances = Helper::Vector::Point::distances(origins, destination);

	int minIndex = Helper::Vector::Float::minIndex(distances);
	
	return distances[minIndex];
}

void Helper::Vector::Point::drawLines(cv::Mat &canvas, const std::vector<cv::Point> &polygon, const bool &closed, const Color &color, const int &thickness, const int &lineType){
	if(polygon.empty())
		return;

	for(auto iter = polygon.begin(); iter != polygon.end() - 1; ++iter)
		cv::line(canvas, *iter, *(iter + 1), color.toScalar(), thickness, lineType);

	if(closed)
		cv::line(canvas, polygon.back(), polygon.front(), color.toScalar(), thickness, lineType);
}

void Helper::Vector::Point::drawFilteredLines(cv::Mat &canvas, const std::vector<cv::Point> &polygon, const std::vector<int> &drawingIndices, const bool &closed, const Color &color, const int &thickness, const int &lineType){
	if(polygon.empty())
		return;

	for(auto iter = drawingIndices.begin(); iter != drawingIndices.end() - 1; ++iter)
		cv::line(canvas, polygon[*iter], polygon[*(iter + 1)], color.toScalar(), thickness, lineType);

	if(closed)
		cv::line(canvas, polygon[drawingIndices.back()], polygon[drawingIndices.front()], color.toScalar(), thickness, lineType);
}