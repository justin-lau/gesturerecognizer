#pragma once

#include <string>
#include "Gesture.h"
#include "Hand.h"

class GestureRecognizer
{
private:
	GestureRecognizer();
	GestureRecognizer(const GestureRecognizer&);

protected:
	static const float openPalmConfidenceAgainst(const Hand* handPtr);
	static const float closedPalmConfidenceAgainst(const Hand* handPtr);
	static const float victorySignConfidenceAginst(const Hand* handPtr);
	static const float okaySignConfidenceAginst(const Hand* handPtr);
	static const float gunSignConfidenceAginst(const Hand* handPtr);
	static const float stopSignConfidenceAginst(const Hand* handPtr);

public:
	// return a std::map containing recognized gesture and confidence
	static const Gesture getGesture(const Hand* handPtr);
};

