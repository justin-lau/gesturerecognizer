#pragma once

#include <string>
#include "Hand.h"

class Gesture
{
protected:
	const Hand* handPtr;
	float confidence;
	std::string name;

public:
	static const float compare(const Gesture &a, const Gesture &b);

	Gesture(const Hand* handPtr, const float &confidence, const std::string name);

	const Hand &getHand() const;
	const float &getConfidence() const;
	const std::string &getName() const;

	void draw(cv::Mat &canvas, cv::Point origin, const bool &withConfidence = false, const Color &color = Color::Black, const int &thickness = 1, const int &fontFace = cv::FONT_HERSHEY_COMPLEX_SMALL, const double &scale = 1.0, const int &lineType = CV_AA) const;
};

