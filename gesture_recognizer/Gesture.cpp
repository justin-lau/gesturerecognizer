#include "Gesture.h"

const float Gesture::compare(const Gesture &a, const Gesture &b){
	return a.confidence - b.confidence;
}

Gesture::Gesture(const Hand* handPtr, const float &confidence, const std::string name) :
	handPtr(handPtr), confidence(confidence), name(name){
}

const Hand &Gesture::getHand() const{
	return *this->handPtr;
}

const float &Gesture::getConfidence() const{
	return this->confidence;
}

const std::string &Gesture::getName() const{
	return this->name;
}

void Gesture::draw(cv::Mat &canvas, cv::Point origin, const bool &withConfidence, const Color &color, const int &thickness, const int &fontFace, const double &scale, const int &lineType) const{
	std::string textToDraw = this->name;

	if(withConfidence)
		textToDraw += " (" + std::to_string((int)(this->confidence * 100)) + "%)";

	int baseLine = 0;
	cv::Size textSize = cv::getTextSize(textToDraw, fontFace, scale, thickness, &baseLine);

	origin = origin - cv::Point(textSize.width / 2, textSize.height / 2);

	cv::putText(canvas, textToDraw, origin, fontFace, scale, color.getInvert().toScalar(), thickness + 5, lineType);
	cv::putText(canvas, textToDraw, origin, fontFace, scale, color.toScalar(), thickness, lineType);
}