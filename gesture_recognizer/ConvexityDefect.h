#pragma once

#include <opencv2\opencv.hpp>
#include "ConvexHull.h"

/**
 * Encapsulates OpenCV's convexity defect type
 */
class ConvexityDefect
{
protected:
	const Contour* contourPtr;
	const ConvexHull* convexHullPtr;

	// index of points from the contour that generates this
	// convexity defect.
	int startIndex;
	int endIndex;
	int farthestIndex;

	// distance from `midPoint` to `end`
	float depth;

	// angle at farthest point in degree
	float angle;

	cv::Point midPoint;

	// distance from `start` to `end`
	float startToEndlength;

public:
	/**
	 * Create instance from OpenCV types
	 */
	ConvexityDefect(const cv::Vec4i &defect, const ConvexHull* convexHullPtr);

	const int &getStartIndex() const;
	const int &getEndIndex() const;
	const int &getFarthestIndex() const;

	const float &getDepth() const;
	const float &getStartToEndLength() const;

	const float &getAngle() const;

	const cv::Point &getMidPoint() const;

	const cv::Point &getStartPoint() const;
	const cv::Point &getEndPoint() const;
	const cv::Point &getFarthestPoint() const;
	const Contour &getContour() const;
	const ConvexHull &getConvexHull() const;

	void drawDepth(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
};

