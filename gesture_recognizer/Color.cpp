#include "Color.h"

const Color &Color::Black = Color(0, 0, 0);
const Color &Color::Red = Color(255, 0, 0);
const Color &Color::Green = Color(0, 255, 0);
const Color &Color::Blue = Color(0, 0, 255);
const Color &Color::Yellow = Color(255, 255, 0);
const Color &Color::Purple = Color(255, 0, 255);
const Color &Color::Cyan = Color(0, 255, 255);
const Color &Color::White = Color(255, 255, 255);

Color::Color(const int &r, const int &g, const int &b){
	this->r = r;
	this->g = g;
	this->b = b;
}

const cv::Scalar Color::toScalar() const{
	return cv::Scalar(this->b, this->g, this->r);
}

const Color Color::getInvert() const{
	return Color(255 - this->r, 255 - this->g, 255 - this->b);
}