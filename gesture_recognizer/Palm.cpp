#include "Helper.h"
#include "Palm.h"

Palm::Palm(const std::vector<cv::Point> &polygon){
	this->polygon = std::vector<cv::Point>(polygon);

	// Calculate centroid using moments
	cv::Moments moments = cv::moments(polygon);
	this->centroid = cv::Point2f(moments.m10 / moments.m00, moments.m01 / moments.m00);

	// Calculate roughtly estimated minimum radius by measuring distance between the
	// centroid and the polygon's vertices
	this->radius = Helper::Vector::Point::minDistance(polygon, this->centroid);
}

const cv::Point &Palm::getCentroid() const{
	return this->centroid;
}

const float &Palm::getRadius() const{
	return this->radius;
}

void Palm::draw(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	cv::circle(canvas, this->centroid, this->radius, color.toScalar(), thickness, lineType);
}

void Palm::drawCentroid(cv::Mat &canvas, const int &radius, const Color &color, const int &thickness, const int &lineType) const{
	cv::circle(canvas, this->centroid, radius, color.toScalar(), thickness, lineType);
}

void Palm::drawPolygon(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	Helper::Vector::Point::drawLines(canvas, this->polygon, true, color, thickness, lineType);
}
