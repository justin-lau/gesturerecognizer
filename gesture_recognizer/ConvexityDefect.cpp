#include "ConvexityDefect.h"
#include "Helper.h"

/**
 * Create instance from OpenCV types
 */
ConvexityDefect::ConvexityDefect(const cv::Vec4i &defect, const ConvexHull* convexHullPtr){
	this->contourPtr = &convexHullPtr->getContour();
	this->convexHullPtr = convexHullPtr;

	this->startIndex = defect[0];
	this->endIndex = defect[1];
	this->farthestIndex = defect[2];
	this->depth = defect[3] / 256.0;

	const cv::Point &start = this->getStartPoint();
	const cv::Point &end = this->getEndPoint();
	const cv::Point &farthest = this->getFarthestPoint();

	this->angle = Helper::Triangle::degreeA(farthest, start, end);

	this->midPoint = Helper::Point::middleOf(start, end);
	this->startToEndlength = Helper::Point::distance(start, end);
}

const int &ConvexityDefect::getStartIndex() const{
	return this->startIndex;
}

const int &ConvexityDefect::getEndIndex() const{
	return this->endIndex;
}

const int &ConvexityDefect::getFarthestIndex() const{
	return this->farthestIndex;
}

const float &ConvexityDefect::getDepth() const{
	return this->depth;
}

const float &ConvexityDefect::getStartToEndLength() const{
	return this->startToEndlength;
}

const float &ConvexityDefect::getAngle() const{
	return this->angle;
}

const cv::Point &ConvexityDefect::getMidPoint() const{
	return this->midPoint;
}

const cv::Point &ConvexityDefect::getStartPoint() const{
	return this->contourPtr->getPoint(this->startIndex);
}

const cv::Point &ConvexityDefect::getEndPoint() const{
	return this->contourPtr->getPoint(this->endIndex);
}

const cv::Point &ConvexityDefect::getFarthestPoint() const{
	return this->contourPtr->getPoint(this->farthestIndex);
}

const Contour &ConvexityDefect::getContour() const{
	return *this->contourPtr;
}

const ConvexHull &ConvexityDefect::getConvexHull() const{
	return *this->convexHullPtr;
}

void ConvexityDefect::drawDepth(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	cv::line(canvas, this->midPoint, this->getFarthestPoint(), color.toScalar(), thickness, lineType);
}