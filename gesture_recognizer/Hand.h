#pragma once

#include <vector>
#include "Contour.h"
#include "Palm.h"

class Finger;
class Gesture;

class Hand
{
private:
	Hand(const Hand&);

protected:
	const Contour &contour;
	Palm* palmPtr;
	bool isRightHand;
	std::vector<Finger*> fingerPtrs;

	void createFingers();
	void createPalm();
	void deleteFingers();
	void deletePalm();

public:
	Hand(const Contour &contour);
	~Hand();

	const int getFingerCount() const;
	const std::vector<Finger*> &getFingerPtrs() const;
	const float &getPalmRadius() const;
	const cv::Point &getPalmCentroid() const;

	// returns an array of finger tips to palm centroid distances
	const std::vector<float> getfingerTipsToCentroidDistances() const;

	const std::vector<float> getDistancesBetweenFingers() const;
	const float getTotalDistanceBetweenFingers() const;

	const Gesture getGesture() const;

	void drawPalm(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawPalmCentroid(cv::Mat &canvas, const int &radius = 3, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawPalmPolygon(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawFingers(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawFingerNames(cv::Mat &canvas, const bool & fullName = false, const Color &color = Color::Black, const int &thickness = 1, const int &fontFace = cv::FONT_HERSHEY_COMPLEX_SMALL, const double &scale = 1.0, const int &lineType = CV_AA) const;
	void drawGesture(cv::Mat &canvas, const bool &withConfidence = false, const Color &color = Color::Black, const int &thickness = 1, const int &fontFace = cv::FONT_HERSHEY_COMPLEX_SMALL, const double &scale = 1.0, const int &lineType = CV_AA) const;
};