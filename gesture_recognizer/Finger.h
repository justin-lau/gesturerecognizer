#pragma once

#include <opencv2\opencv.hpp>
#include "ConvexityDefect.h"

class Finger
{
protected:
	std::string name;
	cv::Point tip;
	cv::Point root;
	const ConvexityDefect &defect;

public:
	static enum FingerNames;

	// the maximum angle between two fingers
	static int angleThresHold;

	// maximum distance between two fingers
	static int distanceThreshold;

	// minimum depth of defect
	static int depthThreshold;

	// minimum distance between fingers and palm circle
	static int distanceToPalmCircleThreshold;

	static bool qualify(const ConvexityDefect &defect);

	Finger(const ConvexityDefect &defect, const ConvexityDefect &prevDefect, const bool &isRightHand, const int &name);

	const cv::Point &getTip() const;
	const cv::Point &getRoot() const;
	const ConvexityDefect &getDefect() const;

	void draw(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawName(cv::Mat &canvas, const bool &fullName = false, const Color &color = Color::Black, const int &thickness = 1, const int &fontFace = cv::FONT_HERSHEY_COMPLEX_SMALL, const double &scale = 1.0, const int &lineType = CV_AA) const;
};

