﻿#include "Contour.h"
#include "ConvexHull.h"
#include "ConvexityDefect.h"
#include "Finger.h"
#include "Hand.h"
#include "Helper.h"
#include "GestureRecognizer.h"

void Hand::createFingers(){
	this->deleteFingers();

	const std::vector<ConvexityDefect*> &defectPtrs = this->contour.getDefectPtrs();
	int defectSize = defectPtrs.size();

	// Find thumb and count candidate defects
	float thumbWeight = -1;
	int thumbIndex = -1;
	int candidateDefectsCount = 0;

	for(int i = 0; i < defectSize; ++i){
		// make sure this is a finger
		if(!Finger::qualify(*defectPtrs[i]))
			continue;

		// weight is calculated by defect angle + defect depth + distance between fingers
		float weight = defectPtrs[i]->getAngle();// + defectPtrs[i]->getDepth() + defectPtrs[i]->getStartToEndLength();

		if(weight > thumbWeight){
			thumbWeight = weight;
			thumbIndex = i;
		}

		++candidateDefectsCount;
	}

	// return if no candidates found
	if(candidateDefectsCount == 0){
		return;
	}
	else if(candidateDefectsCount == 1){
		// assume to be right hand if only one candidate defect was found
		isRightHand = true;

		// 1 defect can means two fingers, imagine when performing the `victory` or `gun` gesture.
		// However, with the `thumb up` gesture or only one finger is open, there will also be 1 defect.
		const ConvexityDefect* defectPtr = defectPtrs[thumbIndex];
		const ConvexityDefect* prevPtr = defectPtrs[Helper::Int::trim(thumbIndex + 1, 0, defectSize - 1)];
		const ConvexityDefect* nextPtr = defectPtrs[Helper::Int::trim(thumbIndex - 1, 0, defectSize - 1)];

		bool hasTwoFingers = false;
		
		if(defectPtr->getAngle() > 55 // unlike `victory`
			&& prevPtr->getAngle() + nextPtr->getAngle() > 180) // unlike `gun`
				candidateDefectsCount = 0; // will still push 1 finger;
	}
	else{
		// because contour vertices were generated counter-clockwise,
		// if the next defect of thumb is a valid finger, it should be a left hand
		if(Finger::qualify(*defectPtrs[(thumbIndex + 1) % (defectSize - 1)]))
			isRightHand = false;
		else
			isRightHand = true;
	}

	int sign = (isRightHand ? -1 : 1);

	for(int i = 0; i <= candidateDefectsCount; i++){
		int currentIndex = Helper::Int::trim(thumbIndex + i * sign, 0, defectSize - 1);
		int prevIndex = Helper::Int::trim(currentIndex - sign, 0, defectSize - 1);
		Finger* fingerPtr = new Finger(*defectPtrs[currentIndex], *defectPtrs[prevIndex], isRightHand, i);
		float distance = Helper::Point::distance(fingerPtr->getTip(), this->getPalmCentroid()) - this->getPalmRadius();

		if(distance < Finger::distanceToPalmCircleThreshold)
			delete fingerPtr; // too short
		else{
			this->fingerPtrs.push_back(fingerPtr);

			// We only need 5 fingers.. I guess.
			if(this->fingerPtrs.size() >= 5)
				break;
		}
	}
}

void Hand::createPalm(){
	this->deletePalm();

	const std::vector<ConvexityDefect*> &defectPtrs = this->contour.getDefectPtrs();

	int defectSize = defectPtrs.size();

	std::vector<cv::Point> palmPolygon;

	if(defectSize >= 3){
		for_each(defectPtrs.begin(), defectPtrs.end(), [&](const ConvexityDefect* defectPtr){
			// filter glitches
			if(Finger::qualify(*defectPtr) || defectPtr->getDepth() > 7.5 /*&& defectPtr->getStartToEndLength() > 20*/)
				palmPolygon.push_back(this->contour.getPoint(defectPtr->getFarthestIndex()));
		});
	}

	if(defectSize < 3 || palmPolygon.size() < 3){
		palmPolygon.empty();

		const std::vector<int> & indices = this->contour.getConvexHull().getPointIndices();
		for_each(indices.begin(), indices.end(), [&](const int &index){
			palmPolygon.push_back(this->contour.getPoint(index));
		});
	}

	if(palmPolygon.size() >= 3)
		this->palmPtr = new Palm(palmPolygon);
}

void Hand::deleteFingers(){
	for(auto &fingerPtr : this->fingerPtrs)
		delete fingerPtr;

	fingerPtrs.clear();
}

void Hand::deletePalm(){
	if(this->palmPtr){
		delete palmPtr;
		palmPtr = NULL;
	}
}

Hand::Hand(const Contour &contour): contour(contour), palmPtr(NULL){
	this->createPalm();
	this->createFingers();	
}

Hand::~Hand(){
	this->deleteFingers();
	this->deletePalm();
}

const int Hand::getFingerCount() const{
	return this->fingerPtrs.size();
}

const std::vector<Finger*> &Hand::getFingerPtrs() const{
	return this->fingerPtrs;
}

const float &Hand::getPalmRadius() const{
	return this->palmPtr->getRadius();
}

const cv::Point &Hand::getPalmCentroid() const{
	return this->palmPtr->getCentroid();
}

const std::vector<float> Hand::getfingerTipsToCentroidDistances() const{
	std::vector<float> distances;

	if(this->palmPtr){
		for(const Finger* fingerPtr : this->fingerPtrs)
			distances.push_back(Helper::Point::distance(fingerPtr->getTip(), this->palmPtr->getCentroid()));
	}

	return distances;
}

const std::vector<float> Hand::getDistancesBetweenFingers() const{
	std::vector<float> distances;

	for(auto iter = this->fingerPtrs.begin(); iter != this->fingerPtrs.end() - 1; ++iter)
		distances.push_back(Helper::Point::distance((*iter)->getTip(), (*(iter + 1))->getTip()));		

	return distances;
}

const float Hand::getTotalDistanceBetweenFingers() const{
	float sum = 0;

	for(auto &value : this->getDistancesBetweenFingers())
		sum += value;

	return sum;
}

const Gesture Hand::getGesture() const{
	return GestureRecognizer::getGesture(this);
}

void Hand::drawPalm(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	if(this->palmPtr)
		this->palmPtr->draw(canvas, color, thickness, lineType);
}

void Hand::drawPalmCentroid(cv::Mat &canvas, const int &radius, const Color &color, const int &thickness, const int &lineType) const{
	if(this->palmPtr)
		this->palmPtr->drawCentroid(canvas, radius, color, thickness, lineType);
}

void Hand::drawPalmPolygon(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	if(this->palmPtr)
		this->palmPtr->drawPolygon(canvas, color, thickness, lineType);
}

void Hand::drawFingers(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	for(auto &fingerPtr : this->fingerPtrs)
		fingerPtr->draw(canvas, color, thickness, lineType);
}

void Hand::drawFingerNames(cv::Mat &canvas, const bool &fullName, const Color &color, const int &thickness, const int &fontFace, const double &scale, const int &lineType) const{
	// Only draw the names if all five fingers are present
	if(fingerPtrs.size() == 5){
		for(auto &fingerPtr : this->fingerPtrs)
			fingerPtr->drawName(canvas, fullName, color, thickness, fontFace, scale, lineType);
	}
}

void Hand::drawGesture(cv::Mat &canvas, const bool &withConfidence, const Color &color, const int &thickness, const int &fontFace, const double &scale, const int &lineType) const{
	this->getGesture().draw(canvas, this->getPalmCentroid() + cv::Point(0, this->getPalmRadius() * 2), withConfidence, color, thickness, fontFace, scale, lineType);
}