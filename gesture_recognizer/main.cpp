#include <iostream>
#include <opencv2/opencv.hpp>
#include <OpenNI.h>
#include "ContourCollection.h"
#include "Finger.h"
#include "GestureCollection.h"
#include "Hand.h"
#include "Helper.h"

// Distance threshold
int distanceThresholdMin = 450; // mm
int distanceThresholdMax = 750; // mm
int contourSizeThresholdMin = 4900;

// Drawing colors
const cv::Scalar COLOR_RED = cv::Scalar(0, 0, 255);
const cv::Scalar COLOR_GREEN = cv::Scalar(0, 255, 0);
const cv::Scalar COLOR_BLUE = cv::Scalar(255, 0, 0);
const cv::Scalar COLOR_YELLOW = cv::Scalar(0, 255, 255);
const cv::Scalar COLOR_PURPLE = cv::Scalar(255, 0, 255);
const cv::Scalar COLOR_CYAN = cv::Scalar(255, 255, 0);
const cv::Scalar COLOR_BLUE_50 = cv::Scalar(255, 0, 0, 100);

int pressEnterToContinue(int exitCode)
{
	std::cout << "Press ENTER to continue..." << std::endl;
	std::cin.get();
	return exitCode;
}

// Stop and remove OpenNI &NiTE resources
void destroyOpenniResources(openni::Device &camera, openni::VideoStream &colorVideoStream, openni::VideoStream &depthVideoStream){
	colorVideoStream.stop();
	colorVideoStream.destroy();

	depthVideoStream.stop();
	depthVideoStream.destroy();

	camera.close();
}

// Create openni resources
int createOpenniResources(openni::Device &camera, openni::VideoStream &colorVideoStream, openni::VideoStream &depthVideoStream){
	openni::Status openniStatus;

	// Initialize OpenNI
	if((openniStatus = openni::OpenNI::initialize()) != openni::STATUS_OK){
		std::cout << "Error: Failed to initialize OpenNI." << std::endl;
		return pressEnterToContinue(openniStatus);
	}

	// Open the input device
	if((openniStatus = camera.open(openni::ANY_DEVICE)) != openni::STATUS_OK){
		std::cout << "Error: Failed to open device." << std::endl;
		return pressEnterToContinue(openniStatus);
	}

	// Test if both sensors are available
	if(camera.getSensorInfo(openni::SENSOR_COLOR) == NULL
		|| camera.getSensorInfo(openni::SENSOR_DEPTH) == NULL){
			std::cout << "Error: Both color and depth sensors are required to use this program." << std::endl;
			destroyOpenniResources(camera, colorVideoStream, depthVideoStream);
			return pressEnterToContinue(1);
	}

	// Create video streams from both sensors
	if((openniStatus = colorVideoStream.create(camera,	openni::SENSOR_COLOR)) != openni::STATUS_OK
		|| (openniStatus = colorVideoStream.start()) != openni::STATUS_OK){
			std::cout << "Error: Failed to create or start Color Video Stream." << std::endl;
			destroyOpenniResources(camera, colorVideoStream, depthVideoStream);
			return pressEnterToContinue(openniStatus);
	}

	if((openniStatus = depthVideoStream.create(camera,	openni::SENSOR_DEPTH)) != openni::STATUS_OK
		|| (openniStatus = depthVideoStream.start()) != openni::STATUS_OK){
			std::cout << "Error: Failed to create or start Color Video Stream." << std::endl;
			destroyOpenniResources(camera, colorVideoStream, depthVideoStream);
			return pressEnterToContinue(openniStatus);
	}

	/*
	// Set Depth to Color registration if supported
	if(camera.isImageRegistrationModeSupported(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR)){
	// Test if Depth to Color registration have been set properly
	if((openniStatus = camera.setImageRegistrationMode(openni::IMAGE_REGISTRATION_DEPTH_TO_COLOR)) != openni::STATUS_OK){
	std::cout << "Error: Image Registration Mode cannot be set." << std::endl;
	destroyOpenniResources(camera, colorVideoStream, depthVideoStream);
	return pressEnterToContinue(openniStatus);
	}
	}
	else{
	std::cout << "Error: Depth to Color registration is required for this program to work." << std::endl;
	destroyOpenniResources(camera, colorVideoStream, depthVideoStream);
	return pressEnterToContinue(1);
	}
	//*/

	return 0;
}

int main(int argc, char** argv){
	openni::Device camera;
	openni::VideoStream colorVideoStream;
	openni::VideoStream depthVideoStream;

	int status = createOpenniResources(camera, colorVideoStream, depthVideoStream);
	if(status != 0)
		return status;

	std::string depthWindowName = "Depth";

#ifdef __DEBUG
	cv::namedWindow(depthWindowName);
	// Minimum and maximum distance refer to
	// http://blogs.msdn.com/b/kinectforwindows/archive/2012/01/20/near-mode-what-it-is-and-isn-t.aspx
	cv::createTrackbar("Dist. Min", depthWindowName, &distanceThresholdMin, 3000);
	cv::createTrackbar("Dist. Max", depthWindowName, &distanceThresholdMax, 4000);
	cv::createTrackbar("Contour", depthWindowName, &contourSizeThresholdMin, 10000);
	cv::createTrackbar("Defects De", depthWindowName, &Finger::depthThreshold, 50);
	cv::createTrackbar("Finger Dis", depthWindowName, &Finger::distanceThreshold, 150);
#endif

	// Input
	int key = -1;

#ifdef __DEBUG
	// FPS counter
	time_t startTime = time(NULL);
	int_fast64_t frameCounter = 0;
#endif

	// Main Loop
	while(key != 27){
		openni::VideoFrameRef colorFrame;
		openni::VideoFrameRef depthFrame;

		// Read frames from video streams
		colorVideoStream.readFrame(&colorFrame);
		depthVideoStream.readFrame(&depthFrame);

		cv::Size frameSize(colorFrame.getWidth(), colorFrame.getHeight());

		if(!colorFrame.isValid() 
			|| !depthFrame.isValid()){
				std::cout << "Error: Invalid frame." << std::endl;
				pressEnterToContinue(1);
				break;
		}

		// Calculate world coordinates from depth values
		openni::DepthPixel* depthPixels = (openni::DepthPixel*)depthFrame.getData();
		std::vector<cv::Scalar> worldCoordinates;
		std::vector<uint8_t> maskData;
		for(int y = 0, i = 0; y < frameSize.height; ++y){
			for(int x = 0; x < frameSize.width; ++x, ++i){
				float worldX;
				float worldY;
				float worldZ;

				openni::CoordinateConverter::convertDepthToWorld(depthVideoStream,
					x, y, depthPixels[i], &worldX, &worldY, &worldZ);

				worldCoordinates.push_back(cv::Scalar(worldX, worldY, worldZ));

				// Generate depth mask at the same time
				maskData.push_back(worldZ >= distanceThresholdMin && worldZ <= distanceThresholdMax ? UINT8_MAX : 0);
			}
		}

		// Covert frames to OpenCV Matrix
		cv::Mat colorCVFrame(frameSize.height, frameSize.width, CV_8UC3, (void*)colorFrame.getData(), colorFrame.getStrideInBytes());
		cv::Mat depthCVFrame(frameSize.height, frameSize.width, CV_16UC1, (void*)depthFrame.getData(), depthFrame.getStrideInBytes());

		// Covert Color Frame to BGR to display color correctly
		cv::cvtColor(colorCVFrame, colorCVFrame, CV_RGB2BGR);

		// Nomalize Depth Frame
		cv::Mat normalizeDepthCVFrame;
		cv::normalize(depthCVFrame, normalizeDepthCVFrame, 0, 255, CV_MINMAX, CV_8UC1);

		// Create Depth Mask
		cv::Mat depthMask(frameSize.height, frameSize.width, CV_8UC1, maskData.data());
		cv::bitwise_and(normalizeDepthCVFrame, depthMask, depthMask);

#ifdef __DEBUG
		// Display Depth Mask
		//cv::namedWindow("Mask");
		//cv::imshow("Mask", depthMask);
#endif

		// Blur Mask
		cv::Mat blurredDepthMask;
		cv::medianBlur(depthMask, blurredDepthMask, 11);
		cv::GaussianBlur(blurredDepthMask, blurredDepthMask, cv::Size(7, 7), 0);
		cv::erode(blurredDepthMask, blurredDepthMask, cv::getStructuringElement(cv::MORPH_ELLIPSE, cv::Size(3, 3)));

#ifdef __DEBUG
		// Display Blurred Mask
		cv::namedWindow("Blurred Mask");
		cv::imshow("Blurred Mask", blurredDepthMask);
#endif

		// Convert depth frame to BGR mode so it can be drawn with color later
		cv::cvtColor(normalizeDepthCVFrame, normalizeDepthCVFrame, CV_GRAY2BGR);

		ContourCollection contours(blurredDepthMask, 1.25, contourSizeThresholdMin);
		std::vector<Hand*> handPtrs;

		contours.each([&](const Contour &contour){
#ifdef __DEBUG

			contour.draw(normalizeDepthCVFrame, Color::Red, 2);
			contour.drawConvexHull(normalizeDepthCVFrame, Color::Purple);
			contour.drawConvexityDefects(normalizeDepthCVFrame, Color::Yellow);
#endif

			Hand* handPtr = new Hand(contour);
#ifdef __DEBUG
			handPtr->drawPalm(normalizeDepthCVFrame, Color::Cyan);
			handPtr->drawPalmCentroid(normalizeDepthCVFrame, 3, Color::Cyan);
			handPtr->drawFingers(normalizeDepthCVFrame, Color::Cyan, 1, CV_AA);
			handPtr->drawFingerNames(normalizeDepthCVFrame, false, Color::White);
			handPtr->drawGesture(normalizeDepthCVFrame, true, Color::Blue);
			//handPtr->drawPalmPolygon(normalizeDepthCVFrame, Color::Black);
#endif
			handPtr->drawGesture(colorCVFrame, false, Color::Blue);
			handPtrs.push_back(handPtr);
		});

		// delete hands
		for(auto &handPtr : handPtrs){
			if(handPtr)
				delete handPtr;
		}

		handPtrs.clear();

#ifdef __DEBUG
		// Draw FPS
		float fps = ++frameCounter / difftime(time(NULL), startTime);
		char buff[20];
		sprintf(buff, "FPS:%2.1f", fps);
		cv::putText(normalizeDepthCVFrame, buff, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, .5f, COLOR_PURPLE, 2);
		cv::putText(normalizeDepthCVFrame, buff, cv::Point(10, 20), cv::FONT_HERSHEY_SIMPLEX, .5f, COLOR_BLUE, 1);
#endif

#ifdef __DEBUG
		// Display Depth Frame
		cv::imshow(depthWindowName, normalizeDepthCVFrame);
#endif

		// Display Color Frame
		cv::namedWindow("RGB");
		cv::imshow("RGB", colorCVFrame);

		// press space to pause and any keys to continue
		if(key == (int)' '){
			cv::waitKey();
			key = -1;
		}
		else
			key = cv::waitKey(5);
	}

	// Destroy all OpenCV's Window
	cv::destroyAllWindows();

	destroyOpenniResources(camera, colorVideoStream, depthVideoStream);

	return 0;
}