#pragma once

#include "Gesture.h"

class GestureCollection
{
protected:
	std::vector<Gesture*> gesturePtrs;

public:
	~GestureCollection();

	void push(Gesture* gesture);
	void sortByConfidence();
	const int length() const;
	const bool isEmpty() const;
	const Gesture &first() const;
};

