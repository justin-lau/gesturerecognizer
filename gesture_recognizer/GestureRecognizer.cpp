#include "Finger.h"
#include "GestureRecognizer.h"
#include "GestureCollection.h"
#include "Helper.h"

const float GestureRecognizer::openPalmConfidenceAgainst(const Hand* handPtr){
	float confidence = 1;

	if(handPtr->getFingerCount() < 5)
		confidence = pow(0.5, 5 - handPtr->getFingerCount());

	for(const auto &distance : handPtr->getfingerTipsToCentroidDistances()){
		if(distance < 85)
			confidence *= pow(0.999, 80 - distance);
	}

	return confidence;
}

const float GestureRecognizer::closedPalmConfidenceAgainst(const Hand* handPtr){
	float confidence = 1;

	if(handPtr->getFingerCount() > 0)
		confidence = pow(0.5, handPtr->getFingerCount());

	return confidence;
}

const float GestureRecognizer::victorySignConfidenceAginst(const Hand* handPtr){
	if(handPtr->getFingerCount() != 2)
		return 0;
	
	float expectingAngle = 40;

	std::vector<Finger*> fingerPtrs = handPtr->getFingerPtrs();

	const cv::Point &tipA = fingerPtrs.front()->getTip();
	const cv::Point& tipB = fingerPtrs.back()->getTip();
	cv::Point rootMid = Helper::Point::middleOf(fingerPtrs.front()->getRoot(), fingerPtrs.back()->getRoot());

	return pow(0.99, abs(Helper::Triangle::degreeA(rootMid, tipA, tipB) - expectingAngle));
}

const float GestureRecognizer::okaySignConfidenceAginst(const Hand* handPtr){
	if(handPtr->getFingerCount() != 4)
		return 0;
	
	float expectingTotalLength = 200;

	return pow(0.99, abs(handPtr->getTotalDistanceBetweenFingers() - expectingTotalLength));
}

const float GestureRecognizer::gunSignConfidenceAginst(const Hand* handPtr){
	if(handPtr->getFingerCount() != 2)
		return 0;

	float expectingAngle = 85;

	std::vector<Finger*> fingerPtrs = handPtr->getFingerPtrs();

	const cv::Point &tipA = fingerPtrs.front()->getTip();
	const cv::Point& tipB = fingerPtrs.back()->getTip();
	cv::Point rootMid = Helper::Point::middleOf(fingerPtrs.front()->getRoot(), fingerPtrs.back()->getRoot());

	return pow(0.99, abs(Helper::Triangle::degreeA(rootMid, tipA, tipB) - expectingAngle));
}

const float GestureRecognizer::stopSignConfidenceAginst(const Hand* handPtr){
	if(handPtr->getFingerCount() != 1)
		return 0;

	float expectingAngle = 85;

	return pow(0.99, abs(handPtr->getFingerPtrs().front()->getDefect().getAngle() - expectingAngle));
}

const Gesture GestureRecognizer::getGesture(const Hand* handPtr){
	std::vector<std::pair<float, std::string>> confidenceMap;

	GestureCollection gestures;

	gestures.push(new Gesture(handPtr, GestureRecognizer::openPalmConfidenceAgainst(handPtr), "Open Palm"));
	gestures.push(new Gesture(handPtr, GestureRecognizer::closedPalmConfidenceAgainst(handPtr), "Closed Palm"));
	gestures.push(new Gesture(handPtr, GestureRecognizer::victorySignConfidenceAginst(handPtr), "Victory Sign"));
	gestures.push(new Gesture(handPtr, GestureRecognizer::okaySignConfidenceAginst(handPtr), "OK Sign"));
	gestures.push(new Gesture(handPtr, GestureRecognizer::gunSignConfidenceAginst(handPtr), "Gun Sign"));
	gestures.push(new Gesture(handPtr, GestureRecognizer::stopSignConfidenceAginst(handPtr), "Stop Sign"));

	gestures.sortByConfidence();
	return gestures.first();
}