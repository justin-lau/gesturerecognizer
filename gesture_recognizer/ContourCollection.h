#pragma once

#include <functional>
#include <vector>
#include "Contour.h"

class ContourCollection
{
private:
	ContourCollection(const ContourCollection&);

protected:
	std::vector<Contour*> contourPtrs;

	void deleteContours();

public:
	/**
	 * Create a collection of contours from an image
	 *
	 * @param cv::Mat image The image to extract contours from
	 *
	 * @param double epsilon Parameter specifying the approximation accuracy
	 *						 when simplifying the polygon.
	 *                       This is the maximum distance between the original
     *                       curve and its approximation.
	 *						 Default to 0.
	 *
	 * @param int sizeThreshold Contours detected with size small than this
	 *                          value will not be added to the collection.
	 */
	ContourCollection(const cv::Mat &image, const double &epsilon = 0, const double &sizeThreshold = 4900);
	~ContourCollection();

	void each(std::function<void (const Contour &contour)> function);
};

