#pragma once

#include <opencv2\opencv.hpp>
#include "Color.h"

class Palm
{
protected:
	std::vector<cv::Point> polygon;
	cv::Point centroid;
	float radius;

	void init(cv::Point centroid, float radius);

public:
	Palm(const std::vector<cv::Point> & polygon);

	const cv::Point &getCentroid() const;
	const float &getRadius() const;

	void draw(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawCentroid(cv::Mat &canvas, const int &radius = 3, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawPolygon(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
};

