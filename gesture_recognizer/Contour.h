#pragma once

#include <vector>
#include <opencv2\opencv.hpp>
#include "Color.h"

class ConvexHull;
class ConvexityDefect;

/**
 * Encapsulates Contour
 */
class Contour
{
private:
	Contour(const Contour&);

protected:
	double size;
	std::vector<cv::Point> points;
	ConvexHull* convexHullPtr;

	/**
	 * Approximate the contour polygon with another polygon with less vertices
	 * so that the distance between them is less or equal to the specified precision.
	 *
	 * @param double epsilon Parameter specifying the approximation accuracy.
	 *                       This is the maximum distance between the original
	 *                       curve and its approximation.
	 */
	void approxPoly(double epsilon);

	void deleteConvexHull();

public:
	/**
	 * Creates an instance of Contour.
	 *
	 * @param double epsilon Parameter specifying the approximation accuracy
	 *						 when simplifying the polygon.
	 *                       This is the maximum distance between the original
     *                       curve and its approximation.
	 *						 Default to 0.
	 */
	Contour(const std::vector<cv::Point> &points, const double &epsilon = 0);
	~Contour();

	/**
	 * Calculate the convex hull of this contour
	 */
	void generateConvexHull();

	const double &getSize() const;
	const cv::Point &getPoint(const int &index) const;
	const cv::Point &getFirstPoint() const;
	const cv::Point &getLastPoint() const;
	const std::vector<cv::Point> &getPoints() const;

	const ConvexHull &getConvexHull() const;
	const std::vector<ConvexityDefect*> & getDefectPtrs() const;

	void draw(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawConvexHull(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawConvexityDefects(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
};
