#include "Contour.h"
#include "ConvexHull.h"
#include "Helper.h"

/**
 * Approximate the contour polygon with another polygon with less vertices
 * so that the distance between them is less or equal to the specified precision.
 *
 * @param double epsilon Parameter specifying the approximation accuracy.
 *                       This is the maximum distance between the original
 *                       curve and its approximation.
 */
void Contour::approxPoly(double epsilon){
	cv::approxPolyDP(this->points, this->points, epsilon, true);
}

/**
 * Calculate the convex hull of this contour
 */
void Contour::generateConvexHull(){
	this->deleteConvexHull();
	
	this->convexHullPtr = new ConvexHull(this);
}

void Contour::deleteConvexHull(){
	if(this->convexHullPtr){
		delete this->convexHullPtr;
		this->convexHullPtr = NULL;
	}
}

/**
 * Creates an instance of Contour.
 *
 * @param double epsilon Parameter specifying the approximation accuracy
 *						 when simplifying the polygon.
 *                       This is the maximum distance between the original
 *                       curve and its approximation.
 *						 Default to 0.
 */
Contour::Contour(const std::vector<cv::Point> &points, const double &epsilon){
	this->points = points;
	this->convexHullPtr = NULL;

	// Simplyfy the polygon
	this->approxPoly(epsilon);

	this->size = cv::contourArea(this->points);
}

Contour::~Contour(){
	this->deleteConvexHull();
}

const double & Contour::getSize() const{
	return this->size;
}

const cv::Point &Contour::getPoint(const int &index) const{
	return this->points[index];
}

const cv::Point &Contour::getFirstPoint() const{
	return this->points.front();
}

const cv::Point &Contour::getLastPoint() const{
	return this->points.back();
}

const std::vector<cv::Point> &Contour::getPoints() const{
	return this->points;
}

const ConvexHull & Contour::getConvexHull() const{
	return *(this->convexHullPtr);
}

const std::vector<ConvexityDefect*> & Contour::getDefectPtrs() const{
	return this->convexHullPtr->getDefectPtrs();
}

void Contour::draw(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	Helper::Vector::Point::drawLines(canvas, this->points, true, color, thickness, lineType);
}

void Contour::drawConvexHull(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	if(this->convexHullPtr)
		this->convexHullPtr->draw(canvas, color, thickness, lineType);
}

void Contour::drawConvexityDefects(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	if(this->convexHullPtr)
		this->convexHullPtr->drawDefectsDepth(canvas, color, thickness, lineType);
}