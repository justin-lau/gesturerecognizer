#include "GestureCollection.h"

GestureCollection::~GestureCollection(){
	for(auto &gesturePtr : this->gesturePtrs){
		if(gesturePtr)
			delete gesturePtr;
	}

	this->gesturePtrs.clear();
}

void GestureCollection::push(Gesture* gesture){
	this->gesturePtrs.push_back(gesture);
}

void GestureCollection::sortByConfidence(){
	std::sort(this->gesturePtrs.begin(), this->gesturePtrs.end(), 
	[](const Gesture* ptrA, const Gesture* ptrB)
		-> bool {
			return Gesture::compare(*ptrA, *ptrB) >= 0;
	});
}

const int GestureCollection::length() const{
	return this->gesturePtrs.size();
}

const bool GestureCollection::isEmpty() const{
	return this->gesturePtrs.empty();
}

const Gesture &GestureCollection::first() const{
	return *this->gesturePtrs.front();
}