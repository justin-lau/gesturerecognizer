#include "ContourCollection.h"

void ContourCollection::deleteContours(){
	for(Contour* &contourPtr : this->contourPtrs)
		delete contourPtr;
	
	this->contourPtrs.clear();
}

/**
 * Create a collection of contours from an image
 *
 * @param cv::Mat image   The image to extract contours from
 * @param double  epsilon Parameter specifying the approximation accuracy
 *						  when simplifying the polygon.
 *                        This is the maximum distance between the original
 *                        curve and its approximation.
 *						  Default to 0.
 */
ContourCollection::ContourCollection(const cv::Mat &image, const double &epsilon, const double &sizeThreshold){
	// Create contours
	std::vector<std::vector<cv::Point>> contourPointsVector;

	// don't do approximation at this point, save a bit of the CPU time
	cv::findContours(image, contourPointsVector, CV_RETR_EXTERNAL, CV_CHAIN_APPROX_NONE);

	for(const auto &contourPoints : contourPointsVector){
		Contour* contour = new Contour(contourPoints, epsilon);

		// filter contours that are too large or too small
		if(contour->getSize() >= sizeThreshold && contour->getSize() <= sizeThreshold * 5){
			contour->generateConvexHull();
			this->contourPtrs.push_back(contour);
		}
		else{
			delete contour;
		}
	}
}


ContourCollection::~ContourCollection(){
	this->deleteContours();
}

void ContourCollection::each(std::function<void (const Contour &contour)> function){
	for(Contour* &contourPtr : this->contourPtrs)
		function(*contourPtr);
}