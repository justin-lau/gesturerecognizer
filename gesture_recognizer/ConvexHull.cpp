#include "ConvexHull.h"
#include "ConvexityDefect.h"
#include "Helper.h"

/**
 * Generate convexity defects
 */
void ConvexHull::generateDefects(){
	//this->deleteDefects();

	cv::vector<cv::Vec4i> defects;

	cv::convexityDefects(this->contourPtr->getPoints(), this->pointIndices, defects);

	for(const cv::Vec4i &defect : defects)
		this->defectPtrs.push_back(new ConvexityDefect(defect, this));
}

void ConvexHull::deleteDefects(){
	for(ConvexityDefect* &defectPtr : this->defectPtrs)
		delete defectPtr;

	this->defectPtrs.clear();
}

ConvexHull::ConvexHull(Contour* contourPtr){
	this->contourPtr = contourPtr;

	cv::convexHull(this->contourPtr->getPoints(), this->pointIndices);

	this->generateDefects();
}

ConvexHull::~ConvexHull(){
	this->deleteDefects();
}

const Contour &ConvexHull::getContour() const{
	return *this->contourPtr;
}

const std::vector<int> &ConvexHull::getPointIndices() const{
	return this->pointIndices;
}

const std::vector<ConvexityDefect*> & ConvexHull::getDefectPtrs() const{
	return this->defectPtrs;
}

void ConvexHull::draw(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	Helper::Vector::Point::drawFilteredLines(canvas, this->contourPtr->getPoints() ,this->pointIndices, true, color, thickness, lineType);
}

void ConvexHull::drawDefectsDepth(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	for_each(this->defectPtrs.begin(), this->defectPtrs.end(), [&](const ConvexityDefect* defectPtr){
		defectPtr->drawDepth(canvas, color, thickness, lineType);
	});
}