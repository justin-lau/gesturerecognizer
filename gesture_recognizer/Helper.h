#pragma once

#include <opencv2\opencv.hpp>
#include "Color.h"

class Helper
{
private:
	Helper();
	Helper(const Helper&);

public:
	class Int{
	private:
		Int();
		Int(const Int&);

	public:
		static const int trim(int value, const int &lowerBound, const int &upperBound);
	};

	class Point{
	private:
		Point();
		Point(const Point&);

	public:
		// Returns the mid point of points `a` and `b`.
		static const cv::Point2f middleOf(const cv::Point &a, const cv::Point &b);

		// Returns the 2D distance of points `a` and `b`.
		static const float distance(const cv::Point &a, const cv::Point &b);

		static const cv::Point extendedPoint(const cv::Point &a, const cv::Point &b);
	};

	class Triangle{
	private:
		Triangle();
		Triangle(const Triangle&);

	public:

		// Finding the angle between line AB and line AC
		static const float degreeA(const cv::Point &pointA, const cv::Point &pointB, const cv::Point &pointC);
	};

	class Vector{
	private:
		Vector();
		Vector(const Triangle&);

	public:
		class Float{
		private:
			Float();
			Float(const Float&);

		public:
			// Returns the index of the element who has the highest value,
			// or -1 if vector is empty.
			static const int maxIndex(const std::vector<float> &vector);

			// Returns the index of the element who has the lowest value,
			// or -1 if vector is empty.
			static const int minIndex(const std::vector<float> &vector);
		};

		class Point{
		private:
			Point();
			Point(const Point&);

		public:
			// Returns a vector of distances calculated from each points in `origins` to `destination`
			static const std::vector<float> distances(const std::vector<cv::Point> &origins, const cv::Point &destination);

			// Returns the minimum distance calculated from each points in `origins` to `destination`,
			// or -1 if vector is empty.
			static const float minDistance(const std::vector<cv::Point> &origins, const cv::Point &destination);

			// Drawlines on the provided `polygon`.
			static void drawLines(cv::Mat &canvas, const std::vector<cv::Point> &polygon, const bool &closed = true, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA);

			// Drawlines on the `polygon`, but only the points that exists in `drawingIndices`.
			static void drawFilteredLines(cv::Mat &canvas, const std::vector<cv::Point> &polygon, const std::vector<int> &drawingIndices, const bool &closed = true, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA);
		};
	};
};