#include "Finger.h"
#include "Helper.h"

enum FingerNames{
	ThumbFinger = 0,
	IndexFinger = 1,
	MiddleFinger = 2,
	RingFinger = 3,
	LittleFinger = 4
};

int Finger::angleThresHold = 130;
int Finger::distanceThreshold = 125;
int Finger::depthThreshold = 15;
int Finger::distanceToPalmCircleThreshold = 20;

bool Finger::qualify(const ConvexityDefect &defect){
	return defect.getAngle() <= Finger::angleThresHold
		&& defect.getDepth() >= Finger::depthThreshold
		&& defect.getStartToEndLength() <= Finger::distanceThreshold;
}

Finger::Finger(const ConvexityDefect &defect, const ConvexityDefect &prevDefect, const bool &isRightHand, const int &name)
	: defect(defect){

		if(isRightHand)
			this->tip = defect.getEndPoint();
		else
			this->tip = defect.getStartPoint();

		this->root = Helper::Point::middleOf(defect.getFarthestPoint(), prevDefect.getFarthestPoint());

		switch(name){
		case ThumbFinger:
			this->name = "Thumb";
			break;
		case IndexFinger:
			this->name = "Index";
			break;
		case MiddleFinger:
			this->name = "Middle";
			break;
		case RingFinger:
			this->name = "Ring";
			break;
		case LittleFinger:
			this->name = "Little";
			break;
		}
}

const cv::Point &Finger::getTip() const{
	return this->tip;
}

const cv::Point &Finger::getRoot() const{
	return this->root;
}

const ConvexityDefect &Finger::getDefect() const{
	return this->defect;
}

void Finger::draw(cv::Mat &canvas, const Color &color, const int &thickness, const int &lineType) const{
	cv::line(canvas, this->root, Helper::Point::extendedPoint(this->root, this->tip), color.toScalar(), thickness, lineType);
	cv::circle(canvas, this->tip, 3, Color::Cyan.toScalar(), thickness, lineType);
	cv::circle(canvas, this->root, 3, Color::Cyan.toScalar(), thickness, lineType);
}

void Finger::drawName(cv::Mat &canvas, const bool & fullName, const Color &color, const int &thickness, const int &fontFace, const double &scale, const int &lineType) const{
	cv::Point origin = Helper::Point::middleOf(Helper::Point::middleOf(this->tip, Helper::Point::extendedPoint(this->root, this->tip)), this->tip);
	
	std::string textToDraw = fullName ? this->name : this->name.substr(0, 1);

	int baseLine = 0;
	cv::Size nameSize = cv::getTextSize(textToDraw, fontFace, scale, thickness, &baseLine);

	origin = origin - cv::Point(nameSize.width / 2, nameSize.height / 2);

	cv::putText(canvas, textToDraw, origin, fontFace, scale, color.getInvert().toScalar(), thickness + 5, lineType);
	cv::putText(canvas, textToDraw, origin, fontFace, scale, color.toScalar(), thickness, lineType);
}