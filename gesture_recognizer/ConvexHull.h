#pragma once

#include <vector>
#include <opencv2\opencv.hpp>
#include "Contour.h"

class ConvexityDefect;

/**
 * Encapsulates a convex hull
 */
class ConvexHull
{
private:
	ConvexHull(const ConvexHull&);

protected:
	Contour* contourPtr;
	std::vector<int> pointIndices;
	std::vector<ConvexityDefect*> defectPtrs;

	/**
	 * Generate convexity defects
	 */
	void generateDefects();

	void deleteDefects();

public:
	ConvexHull(Contour* contourPtr);
	~ConvexHull();

	const Contour &getContour() const;
	const std::vector<int> &getPointIndices() const;
	const std::vector<ConvexityDefect*> &getDefectPtrs() const;

	void draw(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
	void drawDefectsDepth(cv::Mat &canvas, const Color &color = Color::Black, const int &thickness = 1, const int &lineType = CV_AA) const;
};

