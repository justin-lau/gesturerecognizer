# Gesture Recognizer #

## Description ##

Gesture Recognizer is a cross-platoform C++ program that recognizes hand gestures from depth image stream came from a depth sensor. It is created as my final year project from Jinan University. It is experimental and certainly not optimized for serious use.

It made use of OpenNI2 as the interface to communicate with the sensing device, and OpenCV in a large part of the algorithm used to extract interested information from the depth sensor frame.

## Usage ##

### Windows ###

Make sure the sensing device is plugged in and working, start `gesture_recognizer.exe` and every gestures preconfigured into the system will be recognized automatically when you place your palm within the range, which by default to be 45cm ~ 75cm.

#### Debug Version ####

Debug version of the program also display a normalized depth image which aids the user visually in recognizing the range of sensing. It also comes with a couple of handle bars on the user interface that allows different threshold values to be set dynamically. There is, however, no way to directly save the modified value as a configuration at the moment. Any tweaks of these threshold values have to be done in the source code and recompilation is needed to make them work.

## Building ##

### Windows ###

Building on Windows requires Visual Studio 2012, Express version is fine, which runs on Windows 7, Windows Server 2008 R2, or Windows 8. Currently only 64bit versions is configured and tested.

To configure, open `gesture_recognizer.sln` with VS2012, then follow the procedure below:
1. Right click on `gesture_recognizer` in the Solution Explorer, and choose `Properties`.

2. Switch to `All Configurations` at the top of the Property Pages.

3. Under `Configuration Properties > C/C++ > General`, modify `Additional Include Directories` so the paths point to the location of your OpenNI2 and OpenCV `include` directories.

4. Similar to the last step, under `Configuration Properties > Linker > General`, modify `Additional Library Directories` so the paths point to your OpenNI2 and OpenCV `lib` directories.

5. If your OpenCV version is differ than 2.4.5, under `Configuration Properties > Linker > Input`, modify `Additional Dependencies` so that the `opencv_*.lib` have correct namings with the matching versions.

Your built executables will be located in `build/x64/vc11.0/` under either `debug` or `release` configuration you have set when building.

### Linux or Mac OS X ###

Currently the build script for Linux and Mac OS X is not provided. You must create the build scripts yourself in order to build under these operating systems.

## License ##

Copyright (C) 2013 Justin Lau (justin@tclau.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.